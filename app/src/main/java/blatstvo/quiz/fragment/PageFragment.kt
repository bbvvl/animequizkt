package blatstvo.quiz.fragment


import android.content.Context
import android.content.res.Configuration
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintSet
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import blatstvo.quiz.R
import blatstvo.quiz.database.FirebaseStorageLab
import blatstvo.quiz.database.QuestionsLab
import java.util.*


class PageFragment : Fragment() {

    private var mCountAnswers: Int = 0
    private var mPageNumber: Int = 0

    private var mCallback: Callback? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mPageNumber = arguments!!.getInt(POSITION)
        mCountAnswers = arguments!!.getInt(COUNT_ANSWERS)


    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment, null)
        FirebaseStorageLab.setImage(this@PageFragment.context!!, view.findViewById<ImageView>(R.id.view), arguments!!.getInt(DRAWABLE), view.findViewById<ProgressBar>(R.id.progressBar))
        setCountAnswers(view, mCountAnswers)
        setAnswersAndButtonClickListener(view)

        return view
    }


    interface Callback {

        fun onClick(pageNumber: Int, v: View)
    }

    private fun setAnswersAndButtonClickListener(v: View) {
        val answers = arguments!!.getStringArrayList(ANSWERS)

        for (i in 1..mCountAnswers) {
            val view = v.findViewById<Button>(resources.getIdentifier("btn" + i, "id", activity!!.packageName))

            (view as Button).text = answers!![i - 1]
            view.setOnClickListener { v1 ->
                setNotClickable(v)

                val d = v1.background
                if ((v1 as Button).text == QuestionsLab.mQuestions!!.names[arguments!!.getInt(DRAWABLE)]) {
                    d.setColorFilter(resources.getColor(R.color.correct), PorterDuff.Mode.MULTIPLY)
                } else {
                    d.setColorFilter(resources.getColor(R.color.incorrect), PorterDuff.Mode.MULTIPLY)
                    (1..mCountAnswers)
                            .map { v.findViewById<Button>(resources.getIdentifier("btn" + it, "id", activity!!.packageName)) }
                            .forEach {
                                if (it.text == QuestionsLab.mQuestions!!.names[arguments!!.getInt(DRAWABLE)]) {
                                    val d1 = it.background
                                    d1.setColorFilter(resources.getColor(R.color.correct), PorterDuff.Mode.MULTIPLY)
                                }
                            }
                }
                mCallback!!.onClick(mPageNumber, v1)
            }
        }
    }

    private fun setNotClickable(v: View) {
        (1..mCountAnswers)
                .map { v.findViewById<View>(resources.getIdentifier("btn" + it, "id", activity!!.packageName)) }
                .forEach {

                    it.isClickable = false
                }
    }

    private fun setCountAnswers(v: View, count: Int) {
        var countAnswers = count

        val root = v.findViewById<View>(R.id.fragment_answers_constraint_layout) as ConstraintLayout

        val constraintSet = ConstraintSet()
        constraintSet.clone(root)

        if (countAnswers % 2 == 0) {
            for (i in 8 downTo countAnswers + 1) {
                constraintSet.setVerticalWeight(resources.getIdentifier("btn" + i, "id", activity!!.packageName), 0f)
            }
            constraintSet.applyTo(root)
        } else {
            countAnswers++
            for (i in 8 downTo countAnswers + 1) {
                constraintSet.setVerticalWeight(resources.getIdentifier("btn" + i, "id", activity!!.packageName), 0f)
            }
            constraintSet.applyTo(root)
            val btn = v.findViewById<View>(resources.getIdentifier("btn" + countAnswers, "id", activity!!.packageName))
            btn.visibility = View.INVISIBLE//btn.setEnabled(false);

        }


        val constraintSet2 = ConstraintSet()
        constraintSet2.clone(v.findViewById<View>(R.id.fragment_constraint_layout) as ConstraintLayout)


        constraintSet2.setGuidelinePercent(R.id.fragment_guidline,
                if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT)
                    1 - resources.getFraction(R.fraction.fragment_answers_btn_height, 1, 1) * countAnswers
                else
                    0.6f)

        constraintSet2.applyTo(v.findViewById<View>(R.id.fragment_constraint_layout) as ConstraintLayout)

    }

    fun setHalfCountAnswers(list: List<Int>, answer: String) {


        var value = true
        for (i in list.indices) {

            val btn = view!!
                    .findViewById<View>(resources
                            .getIdentifier("btn" + list[i], "id", activity!!.packageName)) as Button


            if (answer == btn.text) {
                value = false
                continue
            }

            btn.isEnabled = false
            if (value && i == list.size - 2) break

        }

    }

    fun setAnswer(answer:Int) {

        setNotClickable(view!!)

        val v1 = view!!.findViewById<Button>(answer)
        val d = v1.background
        if ((v1 as Button).text == QuestionsLab.mQuestions!!.names[arguments!!.getInt(DRAWABLE)]) {
            d.setColorFilter(resources.getColor(R.color.correct), PorterDuff.Mode.MULTIPLY)
        } else {
            d.setColorFilter(resources.getColor(R.color.incorrect), PorterDuff.Mode.MULTIPLY)
            (1..mCountAnswers)
                    .map { view!!.findViewById<View>(resources.getIdentifier("btn" + it, "id", activity!!.packageName)) }
                    .forEach {
                        if ((it as Button).text == QuestionsLab.mQuestions!!.names[arguments!!.getInt(DRAWABLE)]) {
                            val d1 = it.background
                            d1.setColorFilter(resources.getColor(R.color.correct), PorterDuff.Mode.MULTIPLY)
                        }
                    }
        }
        //mCallback!!.onClick(mPageNumber, v1)

    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mCallback = context as Callback?
    }

    override fun onDetach() {
        super.onDetach()
        mCallback = null
    }

    companion object {


        private val DRAWABLE = "drawable"
        private val ANSWERS = "answers"
        private val POSITION = "position"
        private val COUNT_ANSWERS = "count_answers"
        private val ANSWER = "answer"

        fun newInstance(drawable: Int, answers: ArrayList<String>, position: Int, countAnswers: Int//, answer: Int
        ): PageFragment {
            val pageFragment = PageFragment()
            val arguments = Bundle()
            arguments.putInt(DRAWABLE, drawable)
            arguments.putInt(POSITION, position)
            arguments.putInt(COUNT_ANSWERS, countAnswers)
//            arguments.putInt(ANSWER, answer)
            arguments.putStringArrayList(ANSWERS, answers)
            pageFragment.arguments = arguments
            return pageFragment
        }
    }

}
