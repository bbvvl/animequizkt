package blatstvo.quiz.fragment

import android.os.Bundle
import android.support.v7.preference.PreferenceFragmentCompat
import android.support.v7.preference.SeekBarPreference
import blatstvo.quiz.R
import blatstvo.quiz.constants.SEEKBAR_PREFENCE_KEY_CHOICES
import blatstvo.quiz.constants.SEEKBAR_PREFENCE_KEY_QUESTIONDS
import blatstvo.quiz.database.QuestionsLab


class SettingsFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)

        val sb = findPreference(SEEKBAR_PREFENCE_KEY_CHOICES) as SeekBarPreference
        sb.min = 4

        val sb1 = findPreference(SEEKBAR_PREFENCE_KEY_QUESTIONDS) as SeekBarPreference
        sb1.min = 10
        sb1.max = QuestionsLab.mQuestions!!.size()
    }


}