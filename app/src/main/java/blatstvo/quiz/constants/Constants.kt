package blatstvo.quiz.constants

import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference

val DEBUG = false
val COUNT_ANSWERS = "count_answers"
val COUNT_QUESTIONS = "count_questions"

val FIREBASESTORAGE = "gs://animequiz-228.appspot.com"
val storageReference: StorageReference = FirebaseStorage.getInstance(FIREBASESTORAGE).reference
val QUESTIONS_COLLECTION = "questions"
val QUEST_NAME_KEY = "name"
val QUEST_IMAGE_URL_KEY = "imageUrl"

val RESULT_COLLECTION = "results"
val RESULT_NAME_KEY = "name"
val RESULT_TIPS_KEY = "tips"
val RESULT_CA_KEY = "ca"
val RESULT_CQ_KEY = "cq"
val RESULT_POINTS_KEY = "points"

val SEEKBAR_PREFENCE_KEY_CHOICES = "seekbar_preference_key_choices"
val SEEKBAR_PREFENCE_KEY_QUESTIONDS = "seekbar_preference_key_questions"

val POINTS = "points"
val CORRECT_ANSWERS = "ca"
val TIPS = "tips"
