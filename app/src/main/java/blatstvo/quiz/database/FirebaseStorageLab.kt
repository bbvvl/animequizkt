package blatstvo.quiz.database

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import blatstvo.quiz.constants.storageReference
import blatstvo.quiz.glide.GlideApp
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target

object FirebaseStorageLab {

//    val list = SparseArray<Drawable>()

    fun setImage(context: Context, view: ImageView, position: Int, pb: ProgressBar) {

        //if (list[position] == null)
            GlideApp.with(context).load(storageReference.child(QuestionsLab.mQuestions!!.imageUrls[position]))

                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {

                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
  //                          list.put(position, resource!!)
                            pb.visibility = View.GONE
                            return false
                        }
                    })
                    .into(view)
        //else GlideApp.with(context).load(list[position]).into(view)
    }
}