/*
package blatstvo.quiz.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.CursorWrapper
import android.database.sqlite.SQLiteDatabase
import android.util.SparseArray

import java.util.ArrayList

import blatstvo.quiz.database.table.QuizDbSchema.QuizTable.Cols
import blatstvo.quiz.database.table.AnswersDbSchema
import blatstvo.quiz.database.table.TipsDbSchema

import blatstvo.quiz.database.table.QuizDbSchema.QuizTable.Name

class QuizLab private constructor(context: Context) {

    private val mContext: Context = context.applicationContext
    private val mDatabase: SQLiteDatabase

    val lastResult: Quiz
        get() {


            val cursor = mDatabase.query(Name, null//(8) + ""
                    , null, null, null, null, null)

            if (cursor.count == 0) return Quiz()
            cursor.moveToLast()
            val cw = CursorWrapper(cursor)


            val v = Quiz(
                    cw.getInt(cw.getColumnIndex(Cols.getCOUNT_QUESTIONS())),
                    cw.getInt(cw.getColumnIndex(Cols.getCOUNT_TIPS())),
                    cw.getString(cw.getColumnIndex(Cols.getQUIZ())),
                    cw.getString(cw.getColumnIndex(Cols.getUSER())),
                    cw.getInt(cw.getColumnIndex(Cols.getPOINTS()))
            )

            cursor.close()


            return v
        }

    val currentSession: Quiz
        get() {


            val cursor = mDatabase.query(Name, null, WHERE_CLAUSE_ID, ID_CS, null, null, null)

            if (cursor.count == 0) return Quiz()
            println("WTF")
            cursor.moveToFirst()
            val cw = CursorWrapper(cursor)


            val v = Quiz(
                    cw.getInt(cw.getColumnIndex(Cols.getCOUNT_QUESTIONS())),
                    cw.getInt(cw.getColumnIndex(Cols.getCOUNT_TIPS())),
                    cw.getString(cw.getColumnIndex(Cols.getQUIZ())),
                    cw.getString(cw.getColumnIndex(Cols.getUSER())),
                    cw.getInt(cw.getColumnIndex(Cols.getPOINTS()))
            )

            cursor.close()


            return v
        }

    val currentSessionTips: SparseArray<String>?
        get() {

            val value = SparseArray<String>()
            val cursor = mDatabase.query(TipsDbSchema.TipsTable.Name, null, null, null, null, null, null)

            if (cursor.count == 0) return null

            cursor.moveToFirst()
            val cw = CursorWrapper(cursor)

            for (i in 0 until cursor.count) {
                value.put(cw.getInt(cw.getColumnIndex(TipsDbSchema.TipsTable.Cols.ID)), cw.getString(cw.getColumnIndex(TipsDbSchema.TipsTable.Cols.getTips())))
                cursor.moveToNext()
            }

            cursor.close()
            return value

        }

    val currentSessionListAnswers: SparseArray<List<String>>?
        get() {

            val value = SparseArray<List<String>>()
            val cursor = mDatabase.query(AnswersDbSchema.AnswersTable.Name, null, null, null, null, null, null)

            if (cursor.count == 0) return null

            cursor.moveToFirst()
            val cw = CursorWrapper(cursor)

            for (i in 0 until cursor.count) {

                val v1 = ArrayList<String>()
                v1.add(cw.getString(cw.getColumnIndex(AnswersDbSchema.AnswersTable.Cols.Answer_0)))
                v1.add(cw.getString(cw.getColumnIndex(AnswersDbSchema.AnswersTable.Cols.Answer_1)))
                v1.add(cw.getString(cw.getColumnIndex(AnswersDbSchema.AnswersTable.Cols.Answer_2)))
                v1.add(cw.getString(cw.getColumnIndex(AnswersDbSchema.AnswersTable.Cols.Answer_3)))
                v1.add(cw.getString(cw.getColumnIndex(AnswersDbSchema.AnswersTable.Cols.Answer_4)))
                v1.add(cw.getString(cw.getColumnIndex(AnswersDbSchema.AnswersTable.Cols.Answer_5)))
                v1.add(cw.getString(cw.getColumnIndex(AnswersDbSchema.AnswersTable.Cols.Answer_6)))
                v1.add(cw.getString(cw.getColumnIndex(AnswersDbSchema.AnswersTable.Cols.Answer_7)))

                value.put(cw.getInt(cw.getColumnIndex(AnswersDbSchema.AnswersTable.Cols.ID)), v1)
                cursor.moveToNext()
            }

            cursor.close()

            return value
        }

    val allResults: List<Quiz>
        get() {

            val cursor = mDatabase.query(Name, null, NOT_WHERE_CLAUSE_ID, ID_CS, null, null, Cols.getPOINTS() + " DESC", null
            )

            if (cursor.count == 0) return ArrayList()
            cursor.moveToFirst()
            val cw = CursorWrapper(cursor)

            val v = ArrayList<Quiz>()

            while (true) {
                v.add(Quiz(
                        cw.getInt(cw.getColumnIndex(Cols.getCOUNT_QUESTIONS())),
                        cw.getInt(cw.getColumnIndex(Cols.getCOUNT_TIPS())),
                        cw.getString(cw.getColumnIndex(Cols.getQUIZ())),
                        cw.getString(cw.getColumnIndex(Cols.getUSER())),
                        cw.getInt(cw.getColumnIndex(Cols.getPOINTS()))
                )
                )

                if (cursor.isLast) break
                cursor.moveToNext()
            }


            cursor.close()

            return v
        }

    init {
        mDatabase = QuizBaseHelper(mContext).writableDatabase
    }

    fun updateCurrentSessionTips(tips: SparseArray<String>?) {

        mDatabase.delete(TipsDbSchema.TipsTable.Name, null, null)

        if (tips == null) return

        var i = 0
        var q = 0
        while (i < tips.size()) {

            if (tips.indexOfKey(q) < 0) {
                while (tips.indexOfKey(q) < 0) q++
            }
            val values = ContentValues()
            values.put(TipsDbSchema.TipsTable.Cols.getTips(), tips.get(q))
            values.put(TipsDbSchema.TipsTable.Cols.ID, q)
            mDatabase.insert(TipsDbSchema.TipsTable.Name, null, values)
            i++
            q++
        }
    }

    private fun saveCurrentSessionListAnswers(listSparseArray: SparseArray<List<String>>?) {

        mDatabase.delete(AnswersDbSchema.AnswersTable.Name, null, null)

        if (listSparseArray == null) {
            return
        }

        var i = 0
        var q = 0
        while (i < listSparseArray.size()) {

            if (listSparseArray.indexOfKey(q) < 0) {
                while (listSparseArray.indexOfKey(q) < 0) q++
            }
            val values = getContentValuesForAnswers(listSparseArray.get(q), q)
            mDatabase.insert(AnswersDbSchema.AnswersTable.Name, null, values)
            i++
            q++
        }


    }

    fun updateCurrentSession(c: Quiz, listSparseArray: SparseArray<List<String>>) {
        val values = getContentValues(c)
        mDatabase.update(Name, values, WHERE_CLAUSE_ID, ID_CS)

        saveCurrentSessionListAnswers(listSparseArray)

    }

    fun checkOnExistingCurrentSession(): Boolean {

        val cursor = mDatabase.query(Name, arrayOf(Cols.getQUIZ()), WHERE_CLAUSE_ID, ID_CS, null, null, null)

        try {
            if (cursor.count == 0) return false
            cursor.moveToFirst()
            return cursor.getString(cursor.getColumnIndex(Cols.getQUIZ())) != null
        } finally {
            cursor.close()
        }

    }

    fun updateAllResults(c: Quiz) {

        val values = getContentValues(c)
        mDatabase.insert(Name, null, values)

    }

    companion object {

        private val WHERE_CLAUSE_ID = "_id =?"
        private val NOT_WHERE_CLAUSE_ID = "_id !=?"
        private val ID_CS = arrayOf("1") //current Session

        private var sQuizLab: QuizLab? = null

        operator fun get(context: Context): QuizLab {
            if (sQuizLab == null) {
                sQuizLab = QuizLab(context)


            }
            return sQuizLab
        }

        internal fun getContentValues(quiz: Quiz): ContentValues {
            val contentValues = ContentValues()
            contentValues.put(Cols.getCOUNT_QUESTIONS(), quiz.questions)
            contentValues.put(Cols.getCOUNT_TIPS(), quiz.tips)
            contentValues.put(Cols.getQUIZ(), quiz.quiz)
            contentValues.put(Cols.getUSER(), quiz.user)
            contentValues.put(Cols.getPOINTS(), quiz.points)
            return contentValues
        }

        private fun getContentValuesForAnswers(list: List<String>, id: Int): ContentValues {
            val contentValues = ContentValues()
            for (i in list.indices) {
                contentValues.put(AnswersDbSchema.AnswersTable.Cols.Answer_ + i, list[i])
            }
            contentValues.put(AnswersDbSchema.AnswersTable.Cols.ID, id)
            return contentValues
        }
    }

}
*/
