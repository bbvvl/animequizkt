package blatstvo.quiz.database


class Questions {
    var names: ArrayList<String>
    var imageUrls: ArrayList<String>

    constructor(names: ArrayList<String>, imageUrls: ArrayList<String>) : this() {
        this.names = names
        this.imageUrls = imageUrls
    }

    constructor() {
        names = ArrayList()
        imageUrls = ArrayList()
    }

    fun add(name: String, imageUrl: String) {
        names.add(name)
        imageUrls.add(imageUrl)
    }

    fun size() = names.size
}