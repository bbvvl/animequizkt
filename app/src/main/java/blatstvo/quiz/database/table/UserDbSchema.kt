package blatstvo.quiz.database.table


class UserDbSchema {
    object UsersTable {
        val Name = "users"

        object Cols {
            val ID = "_id"
            val Name = "name"
        }

    }

}
