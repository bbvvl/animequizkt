package blatstvo.quiz.database.table

class TipsDbSchema {
    object TipsTable {
        val Name = "tips"

        object Cols {
            val ID = "_id"
            val tip1 = "tip1"
            val tip2 = "tip2"
        }

    }

}
