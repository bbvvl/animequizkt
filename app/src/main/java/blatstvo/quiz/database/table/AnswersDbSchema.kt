package blatstvo.quiz.database.table

class AnswersDbSchema {
    object AnswersTable {
        val Name = "answers"

        object Cols {
            val ID = "_id"
            val Answer_ = "answer"
            val Answer_0 = "answer0"
            val Answer_1 = "answer1"
            val Answer_2 = "answer2"
            val Answer_3 = "answer3"
            val Answer_4 = "answer4"
            val Answer_5 = "answer5"
            val Answer_6 = "answer6"
            val Answer_7 = "answer7"


        }

    }

}
