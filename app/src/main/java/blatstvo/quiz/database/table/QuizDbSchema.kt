package blatstvo.quiz.database.table

class QuizDbSchema {
    object QuizTable {
        val Name = "quiz"

        object Cols {
            val ID = "_id"
            val QUEST = "quest"
            val ANSWER = "answer"
            val POINTS = "points"
        }
    }
}
