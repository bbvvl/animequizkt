package blatstvo.quiz.database

import android.content.Context
import blatstvo.quiz.R
import blatstvo.quiz.constants.*

import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.coroutines.experimental.async
import java.io.IOException
import kotlin.coroutines.experimental.suspendCoroutine


object FirestoreLab {


    suspend fun getQuestions(): Questions = suspendCoroutine { continuation ->
        getQuestions({ continuation.resume(it) }, { continuation.resumeWithException(IOException()) })
    }


    private fun getQuestions(completionHandler: (Questions) -> Unit, errorHandler: (() -> Unit)? = null) {
        val questions = Questions()
        FirebaseFirestore.getInstance().collection(QUESTIONS_COLLECTION).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                task.result.forEach({
                    questions.add(it.getString(QUEST_NAME_KEY), it.getString(QUEST_IMAGE_URL_KEY))

                    //if (DEBUG) println(it.getString(QUEST_NAME_KEY))
                })
                completionHandler(questions)
            } else {
                if (DEBUG) println("Error getting documents. ${task.exception}")
            }
        }
    }


    suspend fun getResults(): List<Quiz> = suspendCoroutine { continuation ->
        getResults({ continuation.resume(it) })
    }


    private fun getResults(completionHandler: (List<Quiz>) -> Unit, errorHandler: (() -> Unit)? = null) {
        FirebaseFirestore.getInstance().collection(RESULT_COLLECTION)
                .orderBy(RESULT_POINTS_KEY, Query.Direction.DESCENDING)
                .limit(90).get().addOnCompleteListener { task ->
            when (task.isSuccessful) {
                true -> {
                    val results = ArrayList<Quiz>()
                    task.result.mapTo(results) { Quiz(user = it.getString(RESULT_NAME_KEY), points = it.getLong(RESULT_POINTS_KEY).toInt()) }
                    completionHandler(results)
                }
                else -> {
                }
            }
        }
    }


    fun addResult(name: String, points: Int, tips: Int, correct_answers: Int, count_questions: Int) = async {
        val recordMap = HashMap<String, Any>()
        recordMap.put(RESULT_NAME_KEY, name)
        recordMap.put(RESULT_POINTS_KEY, points)
        recordMap.put(RESULT_TIPS_KEY, tips)
        recordMap.put(RESULT_CA_KEY, correct_answers)
        recordMap.put(RESULT_CQ_KEY, count_questions)
        FirebaseFirestore.getInstance().collection(RESULT_COLLECTION).add(recordMap)

    }

    private fun addQuestion(context: Context) = async {

        val mFullList = context.resources.getStringArray(R.array.anime_list)

        for ((i, l) in mFullList.withIndex()) {
            val user = HashMap<String, Any>()
            user.put("name", l)
            user.put("imageUrl", "/drawable/${i + 1}.jpg")
            //println(i)
            FirebaseFirestore.getInstance().collection(QUESTIONS_COLLECTION).add(user)
        }
    }
}
