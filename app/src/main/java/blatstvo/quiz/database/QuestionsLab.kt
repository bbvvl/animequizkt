package blatstvo.quiz.database

import java.util.*


object QuestionsLab {

    var mQuestions: Questions? = null

    fun setQuestions(questions: Questions) {
        mQuestions = questions
    }

    fun randomListWithAnswers(position: Int, count: Int): List<String> {
        val q = LinkedHashSet<String>()
        q.add(mQuestions!!.names[position])
        val random = Random()
        while (q.size < count) {
            val i = random.nextInt(mQuestions!!.size())
            q.add(mQuestions!!.names[i])
        }

        val value = q.toList()
        Collections.shuffle(value)
        return value
    }

    fun randomListWithQuestions(count: Int): List<Int> {

        val random = Random()
        val q = LinkedHashSet<Int>()
        while (q.size < count) {
            q.add(random.nextInt(mQuestions!!.size()))
        }
        return q.toList()
    }
}