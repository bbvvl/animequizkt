package blatstvo.quiz.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import blatstvo.quiz.database.table.AnswersDbSchema
import blatstvo.quiz.database.table.QuizDbSchema
import blatstvo.quiz.database.table.TipsDbSchema
import blatstvo.quiz.database.table.UserDbSchema
import org.jetbrains.anko.db.*

class QuizBaseHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx, Database_Name, null, Version) {


    companion object {
        private val Version = 1
        private val Database_Name = "quiz.db"

        private var instance: QuizBaseHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): QuizBaseHelper {
            if (instance == null) {
                instance = QuizBaseHelper(ctx.applicationContext)
            }
            return instance!!
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
        // Here you create tables

        db.createTable(QuizDbSchema.QuizTable.Name, true, "_id" to INTEGER + PRIMARY_KEY + UNIQUE,
                QuizDbSchema.QuizTable.Cols.QUEST to INTEGER,//сюда пишем mlist quiz activity поэлементно ( mlist[0]-mlist[size-1]
                QuizDbSchema.QuizTable.Cols.ANSWER to INTEGER,
                QuizDbSchema.QuizTable.Cols.POINTS to INTEGER
        )

        //db.insert(Name, null, getContentValues(Quiz(0, 0, null!!, "", -100000)))

        db.createTable(AnswersDbSchema.AnswersTable.Name, true, "_id" to INTEGER + PRIMARY_KEY + UNIQUE,
                AnswersDbSchema.AnswersTable.Cols.Answer_0 to TEXT,
                AnswersDbSchema.AnswersTable.Cols.Answer_1 to TEXT,
                AnswersDbSchema.AnswersTable.Cols.Answer_2 to TEXT,
                AnswersDbSchema.AnswersTable.Cols.Answer_3 to TEXT,
                AnswersDbSchema.AnswersTable.Cols.Answer_4 to TEXT,
                AnswersDbSchema.AnswersTable.Cols.Answer_5 to TEXT,
                AnswersDbSchema.AnswersTable.Cols.Answer_6 to TEXT,
                AnswersDbSchema.AnswersTable.Cols.Answer_7 to TEXT)

        db.createTable(TipsDbSchema.TipsTable.Name, true, "_id" to INTEGER + PRIMARY_KEY,
                TipsDbSchema.TipsTable.Cols.tip1 to INTEGER,
                TipsDbSchema.TipsTable.Cols.tip2 to TEXT)

        db.createTable(UserDbSchema.UsersTable.Name, true, "_id" to INTEGER + PRIMARY_KEY + UNIQUE,
                UserDbSchema.UsersTable.Cols.Name to TEXT
        )

    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // Here you can upgrade tables, as usual
        db.dropTable(QuizDbSchema.QuizTable.Name, true)
        db.dropTable(AnswersDbSchema.AnswersTable.Name, true)
        db.dropTable(TipsDbSchema.TipsTable.Name, true)
        db.dropTable(UserDbSchema.UsersTable.Name, true)
        onCreate(db)

    }
}

// Access property for Context
val Context.database: QuizBaseHelper
    get() = QuizBaseHelper.getInstance(applicationContext)

