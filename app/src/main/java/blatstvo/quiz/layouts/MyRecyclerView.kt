package blatstvo.quiz.layouts

import android.content.Context
import android.support.annotation.Nullable
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.view.View

class MyRecyclerView : RecyclerView {

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs!!, 0)

    constructor(context: Context, @Nullable attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle)

    private var sy: Int = 0
    private var sn: Float = 0f

    override fun onMeasure(widthSpec: Int, heightSpec: Int) {

        val heightSpecNew = if (View.MeasureSpec.getSize(heightSpec) > sy * sn)
            View.MeasureSpec.makeMeasureSpec(getHeight(sy, sn), View.MeasureSpec.AT_MOST)
        else heightSpec
        super.onMeasure(widthSpec, heightSpecNew)
    }

    fun setParams(screenY: Int, setNumber: Float) {
        sy = screenY
        sn = setNumber
    }

    private fun getHeight(y: Int, tf: Float): Int {
        val factor = dpsToPixels(70)//70 dpi height view_item
        var q = (tf * y % factor).toInt()
        q = if (q >= factor / 2) (tf * y / factor + 1).toInt() else (tf * y / factor).toInt()
        return q * factor
    }

    private fun dpsToPixels(dps: Int): Int {
        val scale = context.resources.displayMetrics.density
        return (dps * scale + 0.5f).toInt()
    }
}
