package blatstvo.quiz.adapters


import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView

import blatstvo.quiz.R
import blatstvo.quiz.database.FirestoreLab
import blatstvo.quiz.database.Quiz
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async

class MyAdapter(pb: ProgressBar) : RecyclerView.Adapter<MyAdapter.ViewHolder>() {

    private var mListQuiz: List<Quiz> = ArrayList()

    inner class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {

        private val mPoints: TextView = v.findViewById(R.id.tv_points) as TextView
        private val mName: TextView = v.findViewById(R.id.tv_name) as TextView
        private val mPosition: TextView = v.findViewById(R.id.tv_place) as TextView

        fun bind(position: Int) {
            mPoints.text = mListQuiz[position].points.toString()
            mName.text = (mListQuiz[position].user)
            mPosition.text = "${(position + 1)}."
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): MyAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.dialog_view_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)

    }

    override fun getItemCount(): Int {
        return mListQuiz.size
    }

    init {

        async(UI) {
            mListQuiz = async { FirestoreLab.getResults() }.await()
            pb.visibility = View.GONE
            this@MyAdapter.notifyDataSetChanged()
        }
    }

}
