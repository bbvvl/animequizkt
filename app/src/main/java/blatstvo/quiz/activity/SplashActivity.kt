package blatstvo.quiz.activity


import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import blatstvo.quiz.R
import blatstvo.quiz.database.FirestoreLab
import blatstvo.quiz.database.QuestionsLab
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async


class SplashActivity : AppCompatActivity() {


    private var asyncUI: Deferred<Unit>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        asyncUI = async(UI) {
            QuestionsLab.setQuestions(
                    async {
                        FirestoreLab.getQuestions()
                    }.await()
            )
            val i = Intent(this@SplashActivity, MainActivity::class.java)
            startActivity(i)
            finish()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()

        asyncUI!!.cancel()
    }

}