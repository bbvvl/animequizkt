package blatstvo.quiz.activity

import android.app.Activity
import android.content.Intent
import android.graphics.Point
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.constraint.ConstraintLayout
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.TypedValue
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import blatstvo.quiz.R
import blatstvo.quiz.adapters.MyAdapter
import blatstvo.quiz.constants.*
import blatstvo.quiz.database.FirestoreLab
import blatstvo.quiz.database.database
import blatstvo.quiz.database.table.AnswersDbSchema
import blatstvo.quiz.database.table.QuizDbSchema
import blatstvo.quiz.database.table.TipsDbSchema
import blatstvo.quiz.database.table.UserDbSchema
import blatstvo.quiz.layouts.MyRecyclerView
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select

class MainActivity : AppCompatActivity() {

    private val sSettingsActivityRequestCode = 0
    private val sQuizActivityRequstCode = 1
    private val sSignIn = 2

    private var mCountChoices = 4
    private var mCountQuestions = 10

    private var mNicknameEditText: EditText? = null
    private var mSignInButton: Button? = null

    private var mRecyclerView: MyRecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false)

        mNicknameEditText = findViewById<EditText>(R.id.nicknameEditText)
        mSignInButton = findViewById<Button>(R.id.sign_in_button)

        if (isSignedIn()) {
            mNicknameEditText!!.isEnabled = false
            mNicknameEditText!!.setText(GoogleSignIn.getLastSignedInAccount(this)!!.displayName)
            mSignInButton!!.text = getString(R.string.googleSignOut)
        }

        findViewById<Button>(R.id.sign_in_button).setOnClickListener({

            if (isSignedIn()) {
                signOut()
            } else {
                signIn()
            }
        })

    }

    override fun onResume() {
        super.onResume()
        initAD()

        mCountChoices = PreferenceManager.getDefaultSharedPreferences(this).getInt(SEEKBAR_PREFENCE_KEY_CHOICES, mCountChoices)
        mCountQuestions = PreferenceManager.getDefaultSharedPreferences(this).getInt(SEEKBAR_PREFENCE_KEY_QUESTIONDS, mCountQuestions)
        findViewById<Button>(R.id.continueGameButton).isEnabled = checkOnExistingCurrentSession()

        if (checkOnExistingCurrentSession() && !isSignedIn())
            database.use {
                var user = "Unkown"
                select(UserDbSchema.UsersTable.Name).exec {
                    moveToFirst()
                    val value = getString(getColumnIndex(UserDbSchema.UsersTable.Cols.Name))
                    user = if (value != "") value else user

                    mNicknameEditText!!.setText(user)
                }

            }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.activity_main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.activity_main_menu_settings -> {
            showSettings()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    fun onclick(v: View) {

        when (v.id) {
            R.id.newGameButton -> {

                if (findViewById<Button>(R.id.continueGameButton).isEnabled) {
                    val builder = AlertDialog.Builder(this)

                    builder.setMessage(R.string.current_session)

                    builder.setPositiveButton(android.R.string.ok
                    ) { _, _ ->
                        removeCurrentSession()
                        database.use {
                            insert(UserDbSchema.UsersTable.Name, UserDbSchema.UsersTable.Cols.Name to mNicknameEditText!!.text.toString())
                        }
                        startQuiz()
                    }

                    builder.setNegativeButton(android.R.string.cancel, null)
                    builder.create().show()
                } else {
                    database.use {
                        insert(UserDbSchema.UsersTable.Name, UserDbSchema.UsersTable.Cols.Name to mNicknameEditText!!.text.toString())
                    }
                    startQuiz()
                }

            }

            R.id.continueGameButton -> {
                startQuiz()
            }
            R.id.statisticButton -> {
                showAllResults()
            }
            R.id.settingsButton -> showSettings()
        }
    }

    private fun showSettings() {

        val intent = Intent(this, SettingsActivity::class.java)
        startActivityForResult(intent, sSettingsActivityRequestCode)

    }

    private fun onResults(points: Int, tips: Int, correct_answers: Int, user: String) {

        val builder = AlertDialog.Builder(this)

        val layout = layoutInflater.inflate(R.layout.dialog_view, null)

        builder.setView(layout)
        val ad = builder.create()

        builder.setCancelable(false)
        (layout.findViewById(R.id.cu) as TextView).text = user
        (layout.findViewById(R.id.ca) as TextView).text = "$correct_answers/$mCountQuestions"
        (layout.findViewById(R.id.ut) as TextView).text = tips.toString()
        (layout.findViewById(R.id.pt) as TextView).text = points.toString()
        layout.findViewById<Button>(R.id.pb).setOnClickListener { ad.dismiss() }

        ad.show()
    }

    private fun showAllResults() {

        val builder = AlertDialog.Builder(this@MainActivity)

        builder.setCancelable(false)

        val layout = layoutInflater.inflate(R.layout.dialog_view_all, null)
        layout.layoutParams = ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        mRecyclerView = layout.findViewById(R.id.rv) as MyRecyclerView


        val p = Point()
        windowManager.defaultDisplay.getSize(p)
        val tv = TypedValue()
        resources.getValue(R.dimen.contance_to_dialog_view_all_auto_size, tv, true)

        mRecyclerView!!.setParams(p.y, tv.float)

        mRecyclerView!!.layoutManager = LinearLayoutManager(this@MainActivity)
        mRecyclerView!!.adapter = MyAdapter(layout.findViewById<ProgressBar>(R.id.progressBar))
        builder.setView(layout)
        val ad = builder.create()
        layout.findViewById<Button>(R.id.pb).setOnClickListener { ad.dismiss() }
        ad.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (DEBUG) println("MainActivity onActivityResult")
        when (requestCode) {
            sSettingsActivityRequestCode -> {

                val choices = PreferenceManager.getDefaultSharedPreferences(this).getInt(SEEKBAR_PREFENCE_KEY_CHOICES, 0)
                val questions = PreferenceManager.getDefaultSharedPreferences(this).getInt(SEEKBAR_PREFENCE_KEY_QUESTIONDS, 0)
                if (mCountChoices != choices) {
                    mCountChoices = choices

                    if (DEBUG) println("mCountChoices " + mCountChoices)
                    removeCurrentSession()
                }

                if (mCountQuestions != questions) {
                    mCountQuestions = questions
                    if (DEBUG) println("mCountQuestions " + mCountQuestions)
                    removeCurrentSession()

                }
            }
            sQuizActivityRequstCode -> if (resultCode == Activity.RESULT_OK) {
                var user = "Unkown"
                database.use {
                    select(UserDbSchema.UsersTable.Name).exec {
                        moveToFirst()
                        val value = getString(getColumnIndex(UserDbSchema.UsersTable.Cols.Name))
                        user = if (value != "") value else user
                    }
                }
                removeCurrentSession()
                if (data != null) {
                    onResults(data.getIntExtra(POINTS, 0), data.getIntExtra(TIPS, 0), data.getIntExtra(CORRECT_ANSWERS, 0), user)

                    async(UI) {
                        FirestoreLab.addResult(points = data.getIntExtra(POINTS, 0), tips = data.getIntExtra(TIPS, 0), correct_answers = data.getIntExtra(CORRECT_ANSWERS, 0), name = user, count_questions = mCountQuestions).await()
                    }

                }
            }

            sSignIn -> {
                val result: GoogleSignInResult = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
                if (result.isSuccess) {
                    Toast.makeText(this, getString(R.string.googleSignInS), Toast.LENGTH_SHORT).show()
                    mNicknameEditText!!.setText(result.signInAccount!!.displayName)
                    mNicknameEditText!!.isEnabled = false
                    mSignInButton!!.text = getString(R.string.googleSignOut)
                } else Toast.makeText(this, getString(R.string.googleSignInU), Toast.LENGTH_SHORT).show()

            }
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun signIn() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()

        val signInIntent = GoogleSignIn.getClient(this, gso).signInIntent
        startActivityForResult(signInIntent, sSignIn)
    }

    private fun signOut() {
        val signInClient = GoogleSignIn.getClient(this, GoogleSignInOptions.DEFAULT_SIGN_IN)
        signInClient.signOut().addOnCompleteListener(this) { task ->
            if (task.isSuccessful) {
                Toast.makeText(this, getString(R.string.googleSignOutS), Toast.LENGTH_SHORT).show()
                mNicknameEditText!!.isEnabled = true
                mNicknameEditText!!.setText("")
                mSignInButton!!.text = getString(R.string.googleSignIn)
            } else
                Toast.makeText(this, getString(R.string.googleSignOutU), Toast.LENGTH_SHORT).show()

        }
    }

    private fun isSignedIn(): Boolean {
        return GoogleSignIn.getLastSignedInAccount(this) != null
    }

    private fun initAD() {

//        MobileAds.initialize(this, "ca-app-pub-1335465264150074~3463081027")
        val mAdView = findViewById<AdView>(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        mAdView.loadAd(adRequest)
    }

    private fun startQuiz() {

/*async(UI) {
bg {  }
}*/

        val intent2 = Intent(application, QuizActivity::class.java)
        intent2.putExtra(COUNT_ANSWERS, mCountChoices)
        intent2.putExtra(COUNT_QUESTIONS, mCountQuestions)
        startActivityForResult(intent2, sQuizActivityRequstCode)
    }

    private fun checkOnExistingCurrentSession() = database.use {
        return@use select(QuizDbSchema.QuizTable.Name, QuizDbSchema.QuizTable.Cols.QUEST).exec {
            return@exec count != 0
        }

    }

    private fun removeCurrentSession() {

        database.use {
            delete(AnswersDbSchema.AnswersTable.Name)
            delete(QuizDbSchema.QuizTable.Name)
            delete(TipsDbSchema.TipsTable.Name)
            delete(UserDbSchema.UsersTable.Name)
        }
    }
}
