package blatstvo.quiz.activity


import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.design.widget.TabLayout
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.SparseArray
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import blatstvo.quiz.R
import blatstvo.quiz.constants.*
import blatstvo.quiz.database.QuestionsLab
import blatstvo.quiz.database.database
import blatstvo.quiz.database.table.AnswersDbSchema
import blatstvo.quiz.database.table.QuizDbSchema
import blatstvo.quiz.database.table.TipsDbSchema
import blatstvo.quiz.fragment.PageFragment
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import java.util.*
import kotlin.collections.ArrayList


class QuizActivity : AppCompatActivity(), PageFragment.Callback {

    private var mViewPager: ViewPager? = null
    private var mLayout: TabLayout? = null

    //сопоставление массива и позиций
    private var mList: List<Int>? = null
    private var mAnswers: SparseArray<Int>? = null
    private var mCorrectAnswers: Int = 0

    private var mListWithAnswers: SparseArray<List<String>>? = null
    private var mCurrentTips: SparseArray<String>? = null


    private lateinit var mItemCall: MenuItem
    var mItemPercent: MenuItem? = null
    private var tabpos = 0

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (DEBUG) println("onCreate")
        setContentView(R.layout.activity_quiz)

    }

    public override fun onPause() {
        super.onPause()
        if (DEBUG) println("onPause")
        saveCurrentSession()
        tabpos = mViewPager!!.currentItem

    }


    public override fun onResume() {
        super.onResume()
        if (DEBUG) println("onResume")
        restoreCurrentSession()

        initViewPager()
        mViewPager!!.currentItem = tabpos
    }

    public override fun onStop() {
        super.onStop()

    }

    public override fun onStart() {
        super.onStart()
        if (DEBUG) println("onStart")

    }

    private fun initViewPager() {

        mViewPager = findViewById(R.id.viewpager)
        mViewPager!!.adapter = MyFragmentPagerAdapter(supportFragmentManager, intent.getIntExtra(COUNT_ANSWERS, 4))


        mViewPager!!.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                //var position = position
                if (mItemPercent != null) {
                    mItemPercent!!.isEnabled = (mCurrentTips!!.indexOfKey(position) < 0) and (mAnswers!!.indexOfKey(position) < 0)
                }
            }


            override fun onPageScrollStateChanged(state: Int) {

            }
        })

        mLayout = findViewById(R.id.sliding_tabs)

        mLayout!!.setupWithViewPager(mViewPager)


        for (i in 0 until intent.getIntExtra(COUNT_QUESTIONS, 10)) {

            mLayout!!.getTabAt(i)!!.setCustomView(R.layout.tab_view)

            (mLayout!!.getTabAt(i)!!.customView!!
                    .findViewById(R.id.tab_view_textview) as TextView).text = mViewPager!!.adapter!!.getPageTitle(i)
        }

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (DEBUG) println("onSaveInstanceState")
        outState.putInt(VIEWPAGER_KEY, tabpos)
        //   outState.putIntegerArrayList(wqe, ArrayList(mList))
    }


    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        if (DEBUG) println("onRestoreInstanceState")
        tabpos = savedInstanceState.getInt(VIEWPAGER_KEY)
    }


    override fun onClick(pageNumber: Int, v: View) {

        if (mItemPercent != null)
            mItemPercent!!.isEnabled = false

        mAnswers!!.put(pageNumber, v.id)

        if ((v as Button).text == QuestionsLab.mQuestions!!.names[mList!![pageNumber]]) {
            mCorrectAnswers++
        }



        if (mList!!.size == mAnswers!!.size()) {

            val int = Intent()
            val points = (mCorrectAnswers * (intent.getIntExtra(COUNT_QUESTIONS, 10))
                    - mCurrentTips!!.size() * intent.getIntExtra(COUNT_QUESTIONS, 10) / 2) * intent.getIntExtra(COUNT_ANSWERS, 4)
            int.putExtra(POINTS, points)
            int.putExtra(CORRECT_ANSWERS, mCorrectAnswers)
            int.putExtra(TIPS, mCurrentTips!!.size())
            setResult(Activity.RESULT_OK, int)
            finish()


        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        if (DEBUG) println("onCreateOptionsMenu")
        val inflater = menuInflater
        inflater.inflate(R.menu.activity_quiz_menu, menu)

        mItemCall = menu.findItem(R.id.activity_quiz_menu_call)
        mItemPercent = menu.findItem(R.id.activity_quiz_menu_percent)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (DEBUG) println("onOptionsItemSelected")
        when (item.itemId) {

            R.id.activity_quiz_menu_call -> {

                checkAndRequestPermissions()
                return true
            }

            R.id.activity_quiz_menu_percent -> {
                val list = ArrayList<Int>()
                val ca = intent.getIntExtra(COUNT_ANSWERS, 4)
                val random = Random()
                var i = 0
                while (i < ca / 2 + 1) {
                    val value = random.nextInt(ca) + 1
                    if (!list.contains(value))
                        list.add(value)
                    else
                        i--
                    i++
                }
                ((mViewPager!!.adapter as MyFragmentPagerAdapter)
                        .getRegisteredFragment(mViewPager!!.currentItem) as PageFragment)
                        .setHalfCountAnswers(list, QuestionsLab.mQuestions!!.names[mList!![mViewPager!!.currentItem]])


                mCurrentTips!!.put(mViewPager!!.currentItem, list.toString())

                mItemPercent!!.isEnabled = false
                return true
            }

            else -> return super.onOptionsItemSelected(item)
        }

    }

    private fun checkAndRequestPermissions() {

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {


            if (Build.VERSION.SDK_INT >= 23) {
                requestPermissions(arrayOf(Manifest.permission.READ_CONTACTS, Manifest.permission.CALL_PHONE),
                        PERMISSIONS_REQUEST_READ_CONTACTS)
            }


        } else {
            if (DEBUG) println("readContactAndCall in check")
            readContactAndCall()
        }
        //checkOnExistingCurrentSession();
    }

    private fun readContactAndCall() {

        val intent = Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI)
        startActivityForResult(intent, PICK_CONTACT)

    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            // If request is cancelled, the result arrays are empty.
            if (grantResults.isNotEmpty()
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                run {
                    if (DEBUG) println("readContactAndCall in onRequestPermissionsResult")
                    readContactAndCall()
                }

            } else {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.READ_CONTACTS) || ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.CALL_PHONE)) {
                    val builder = AlertDialog.Builder(this)

                    builder.setMessage(R.string.permission)

                    builder.setPositiveButton(android.R.string.ok
                    ) { _, _ ->
                        if (Build.VERSION.SDK_INT >= 23)
                            requestPermissions(arrayOf(Manifest.permission.READ_CONTACTS, Manifest.permission.CALL_PHONE),
                                    PERMISSIONS_REQUEST_READ_CONTACTS)
                    }

                    builder.setNegativeButton(android.R.string.cancel, null)
                    builder.create().show()

                } else
                    showNoPermissionSnackbar()
            }
        } else
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }


    private fun showNoPermissionSnackbar() {
        Snackbar.make(findViewById(R.id.ConstraintLayout), resources.getString(R.string.snackbar_text), Snackbar.LENGTH_LONG)
                .setAction("Settings") {
                    openApplicationSettings()
                    Toast.makeText(applicationContext,
                            resources.getString(R.string.snackbar_settings_text),
                            Toast.LENGTH_SHORT)
                            .show()
                }
                .show()
    }

    private fun openApplicationSettings() {
        val appSettingsIntent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.parse("package:" + packageName))
        startActivityForResult(appSettingsIntent, PERMISSION_REQUEST_CODE)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        when (requestCode) {
            PERMISSION_REQUEST_CODE -> checkAndRequestPermissions()//readContactAndCall();
            PICK_CONTACT -> {
                if (resultCode == Activity.RESULT_OK) {

                    var name = ""

                    val c = contentResolver.query(data!!.data!!, null, null, null, null)

                    if (c!!.moveToFirst()) {
                        name = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID))
                    }
                    c.close()
                    val cursorPhone = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            arrayOf(ContactsContract.CommonDataKinds.Phone.NUMBER),
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            arrayOf(name), null)

                    if (cursorPhone != null && cursorPhone.moveToFirst()) {
                        name = cursorPhone.getString(0)
                    }


                    cursorPhone!!.close()

                    val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + name))


                    //i can`t skip this :(

                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return
                    }



                    startActivityForResult(intent, CALL_TO_CONTACT)
                }


                mItemCall.isEnabled = false
            }


            CALL_TO_CONTACT -> {
                mItemCall.isEnabled = false
            }
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun saveCurrentSession() {

        //TIPS
        database.use {
            delete(AnswersDbSchema.AnswersTable.Name)
            delete(QuizDbSchema.QuizTable.Name)
            delete(TipsDbSchema.TipsTable.Name)

            var k = 0
            var l = 0
            while (k < mAnswers!!.size()) {

                if (mAnswers!!.indexOfKey(l) < 0) {
                    while (mAnswers!!.indexOfKey(l) < 0) l++
                }

                if (DEBUG) println("mAnswers$l ${mAnswers!![l]}")

                k++
                l++
            }

            var i = 0
            var q = 0
            while (i < mCurrentTips!!.size()) {

                if (mCurrentTips!!.indexOfKey(q) < 0) {
                    while (mCurrentTips!!.indexOfKey(q) < 0) q++
                }
                insert(TipsDbSchema.TipsTable.Name, TipsDbSchema.TipsTable.Cols.ID to q, TipsDbSchema.TipsTable.Cols.tip2 to mCurrentTips!!.get(q))
                i++
                q++
            }
            //if (DEBUG) println("mlist= $mList")

            //ANSWES

            i = 0
            q = 0
            while (i < mListWithAnswers!!.size()) {

                if (mListWithAnswers!!.indexOfKey(q) < 0) {
                    while (mListWithAnswers!!.indexOfKey(q) < 0) q++
                }

                insert(AnswersDbSchema.AnswersTable.Name, AnswersDbSchema.AnswersTable.Cols.ID to q,
                        AnswersDbSchema.AnswersTable.Cols.Answer_0 to mListWithAnswers!![q][0],
                        AnswersDbSchema.AnswersTable.Cols.Answer_1 to mListWithAnswers!![q][1],
                        AnswersDbSchema.AnswersTable.Cols.Answer_2 to mListWithAnswers!![q][2],
                        AnswersDbSchema.AnswersTable.Cols.Answer_3 to mListWithAnswers!![q][3],
                        AnswersDbSchema.AnswersTable.Cols.Answer_4 to if (mListWithAnswers!![q].size >= 5) mListWithAnswers!![q][4] else -1,
                        AnswersDbSchema.AnswersTable.Cols.Answer_5 to if (mListWithAnswers!![q].size >= 6) mListWithAnswers!![q][5] else -1,
                        AnswersDbSchema.AnswersTable.Cols.Answer_6 to if (mListWithAnswers!![q].size >= 7) mListWithAnswers!![q][6] else -1,
                        AnswersDbSchema.AnswersTable.Cols.Answer_7 to if (mListWithAnswers!![q].size >= 8) mListWithAnswers!![q][7] else -1
                )
                i++
                q++
            }
            //QUESTIONS\
            for (z in 0 until mList!!.size) {
                if (mAnswers!!.indexOfKey(z) >= 0)
                    insert(QuizDbSchema.QuizTable.Name, QuizDbSchema.QuizTable.Cols.ID to z, QuizDbSchema.QuizTable.Cols.QUEST to mList!![z], QuizDbSchema.QuizTable.Cols.ANSWER to mAnswers!![z], QuizDbSchema.QuizTable.Cols.POINTS to -1)
                else
                    insert(QuizDbSchema.QuizTable.Name, QuizDbSchema.QuizTable.Cols.ID to z, QuizDbSchema.QuizTable.Cols.QUEST to mList!![z], QuizDbSchema.QuizTable.Cols.ANSWER to -1, QuizDbSchema.QuizTable.Cols.POINTS to -1)
            }

            insert(QuizDbSchema.QuizTable.Name, QuizDbSchema.QuizTable.Cols.POINTS to mCorrectAnswers)
        }
    }

    private fun restoreCurrentSession() {

        if (checkOnExistingCurrentSession()) {

            database.use {

                mList = ArrayList()
                mAnswers = SparseArray()

                select(QuizDbSchema.QuizTable.Name).exec {
                    moveToFirst()
                    for (i in 0 until count) {

                        val z = getInt(getColumnIndex(QuizDbSchema.QuizTable.Cols.POINTS))
                        if (z != -1) {
                            mCorrectAnswers = z
                            continue
                        }

                        (mList as ArrayList<Int>).add(getInt(getColumnIndex(QuizDbSchema.QuizTable.Cols.QUEST)))

                        val q = getInt(getColumnIndex(QuizDbSchema.QuizTable.Cols.ANSWER))
                        if (q != -1) mAnswers!!.put(getInt(getColumnIndex(QuizDbSchema.QuizTable.Cols.ID)), q)

                        moveToNext()
                    }

                }

                mCurrentTips = SparseArray()

                select(TipsDbSchema.TipsTable.Name).exec {
                    moveToFirst()
                    for (i in 0 until count) {
                        mCurrentTips!!.put(getInt(getColumnIndex(TipsDbSchema.TipsTable.Cols.ID)), getString(getColumnIndex(TipsDbSchema.TipsTable.Cols.tip2)))
                        moveToNext()
                    }

                }
                select(AnswersDbSchema.AnswersTable.Name).exec {

                    mListWithAnswers = SparseArray()
                    moveToFirst()
                    for (i in 0 until count) {

                        val v1 = ArrayList<String>()

                        (0 until intent.getIntExtra(COUNT_ANSWERS, 4)).mapTo(v1) { getString(getColumnIndex(AnswersDbSchema.AnswersTable.Cols.Answer_ + it)) }
                        mListWithAnswers!!.put(getInt(getColumnIndex(AnswersDbSchema.AnswersTable.Cols.ID)), v1)
                        moveToNext()
                    }
                }
//
            }

            var k = 0
            var l = 0
            while (k < mAnswers!!.size()) {

                if (mAnswers!!.indexOfKey(l) < 0) {
                    while (mAnswers!!.indexOfKey(l) < 0) l++
                }

                if (DEBUG) println("mAnswers$l ${mAnswers!![l]}")

                k++
                l++
            }
        } else {
            if (DEBUG) println("THIS IS NULL")
            mList = QuestionsLab.randomListWithQuestions(intent.getIntExtra(COUNT_QUESTIONS, 10))

            mAnswers = SparseArray()
            mListWithAnswers = SparseArray()
            mCurrentTips = SparseArray()
        }

    }

    private fun checkOnExistingCurrentSession() = database.use {
        return@use select(QuizDbSchema.QuizTable.Name, QuizDbSchema.QuizTable.Cols.QUEST).exec {
            return@exec count != 0
        }

    }

    private inner class MyFragmentPagerAdapter internal constructor(fm: FragmentManager, internal var mCount: Int) : FragmentPagerAdapter(fm) {

        internal var registeredFragments = SparseArray<Fragment>()

        override fun getItem(position: Int): Fragment {
            //println("getItem")
            if (mListWithAnswers!!.get(position) == null)
                mListWithAnswers!!.append(position, QuestionsLab.randomListWithAnswers(mList!![position], mCount))

            return PageFragment.newInstance(mList!![position], mListWithAnswers!!.get(position) as ArrayList<String>, position, mCount)
        }

        override fun getCount(): Int {
            return intent.getIntExtra(COUNT_QUESTIONS, 10)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return (position + 1).toString()
        }

        override fun instantiateItem(container: ViewGroup, position: Int): Any {

            //println("instantiateItem " + position)
            val fragment = super.instantiateItem(container, position) as Fragment
            registeredFragments.put(position, fragment)
            return fragment
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            //println("destroyItem " + position)
            registeredFragments.remove(position)
            super.destroyItem(container, position, `object`)
        }

        override fun finishUpdate(contain: ViewGroup) {
            super.finishUpdate(contain)

//            println("finishUpdate " + mViewPager!!.currentItem)

            val list = ArrayList<Int>()
            if (mViewPager!!.currentItem > 0)
                list.add(mViewPager!!.currentItem - 1)
            list.add(mViewPager!!.currentItem)

            if (mViewPager!!.currentItem < intent.getIntExtra(COUNT_QUESTIONS, 10) - 1)
                list.add(mViewPager!!.currentItem + 1)

            for (i in list) {
                if (registeredFragments.get(i) == null) continue

                if (mCurrentTips!!.indexOfKey(i) >= 0) {

                    var s = mCurrentTips!!.get(i)
                    s = s.substring(s.indexOf('[') + 1, s.indexOf(']'))

                    val values = ArrayList<Int>()
                    while (true) {
                        if (s.length == 1) {
                            values.add(Integer.parseInt(s))
                            break
                        } else {
                            values.add(Integer.parseInt(s.substring(0, s.indexOf(','))))
                        }
                        s = s.substring(s.indexOf(' ') + 1)
                    }
                    (registeredFragments.get(i) as PageFragment)
                            .setHalfCountAnswers(values, QuestionsLab.mQuestions!!.names[mList!![i]])
                }

                if (mAnswers!!.indexOfKey(i) >= 0) {
                    (registeredFragments.get(i) as PageFragment).setAnswer(mAnswers!![i])
                }
            }
        }

        internal fun getRegisteredFragment(position: Int): Fragment {
            return registeredFragments.get(position)
        }

    }

    companion object {


        private val PERMISSIONS_REQUEST_READ_CONTACTS = 0
        private val PERMISSION_REQUEST_CODE = 1
        private val PICK_CONTACT = 2
        private val CALL_TO_CONTACT = 3
        private val VIEWPAGER_KEY = "viewpager_key"


    }
}
